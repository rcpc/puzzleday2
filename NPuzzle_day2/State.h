#pragma once

#include "types.h"
#include <array>
#include <optional>
#include <map>
#include <numeric>
#include <unordered_map>
#include <functional>
#include <unordered_set>

template <size_t N = 3>
class State
{
public:
	using ElementType = uint8_t;

private: // Types
	using Position2D = std::pair<size_t, size_t>;

public:
	static const size_t Dimension = N;
	using Data = std::array<ElementType, N * N>;

	State() = delete;
	State(Data data) : m_data{ std::move(data) } {};

	
	auto GetRowIteratorBegin(size_t rowIndex) const
	{
		return m_data.begin() + Dimension * rowIndex;
	}
	auto GetRowIteratorEnd(size_t rowIndex) const
	{
		return GetRowIteratorBegin(rowIndex) + Dimension;
	}

	/* Custom iterator s*/
	class ColumnIterator : public std::iterator<std::input_iterator_tag,
		ElementType,
		typename Data::difference_type,
		typename Data::pointer,
		typename Data::reference>
	{
	public:
		ColumnIterator(typename Data::iterator begin, typename Data::iterator end)
			: m_begin(begin), m_end(end)
		{/* Empty */}
		bool operator == (const ColumnIterator& other)
		{
			return m_it == other.m_it;
		}
		bool operator != (const ColumnIterator& other)
		{
			return !(*this == other);
		}
		ColumnIterator& operator ++ ()
		{
			if (std::distance(m_it, m_end) > Dimension)
				m_it = m_end;
			else
				m_it += Dimension;
			return *this;
		}
		ColumnIterator operator ++ (int)
		{
			ColumnIteartor copy = *this;
			++(*this);
			return copy;
		}
		const ElementType& operator* () const
		{
			return *m_it;
		}

	private:
		typename Data::iterator m_it;
		typename Data::iterator m_end;
	};

	ColumnIterator GetColumnIteratorBegin(size_t column) const
	{
		return ColumnIterator(m_data.begin() + column, m_data.end());
	}
	 
	ColumnIterator GetColumnIteratorEnd(size_t column) const
	{
		return ColumnIterator(m_data.begin(), m_data.end());
	}
	
	const Data& GetData() const
	{
		return m_data;
	}
	static const State& GoalState()
	{
		static auto generateGoalState = []()
		{
			Data solvedData;
			std::iota(solvedData.begin(), solvedData.end(), 1);
			solvedData.back() = 0;
			return State(std::move(solvedData));
		};
		static State goalState{ generateGoalState() };
		return goalState;
	}

	bool IsGoalState() const
	{
		return m_data == GoalState().m_data;
	}
	bool IsValid() const
	{
		auto goalState = GoalState();
		return std::is_permutation(m_data.begin(), m_data.end(), goalState.GetData().begin());

	}
	bool IsSolvable() const
	{
		auto countInversions = [](auto begin, auto end)
		{
			size_t acc{ 0u };
			for (auto it = begin; it != end; ++it)
			{
				auto&& current = *it;
				if (current != 0)
					acc += std::count_if(it, end, [current](auto next) { return next != 0 && next < current; });
			}

			return acc;
		};

		const auto inversionsCount = countInversions(m_data.begin(), m_data.end());
		const auto isInversionCountEven = inversionsCount % 2 == 0;
		const bool isNOdd = N % 2 == 1;
		const bool isBlankRowEven = GetBlankPosition2D().first % 2 == 0;

		return (isNOdd) ? isInversionCountEven :
			(isBlankRowEven) ? !isInversionCountEven :
			isInversionCountEven;
	}

	std::vector<std::pair<State, MoveDirection>> GetChildren() const
	{
		static auto allMoves = { MoveDirection::LEFT, MoveDirection::RIGHT, MoveDirection::UP, MoveDirection::DOWN };
		std::vector<std::pair<State, MoveDirection>> children;

		for (auto direction : allMoves)
		{
			std::optional<State> child = Move(direction);
		
			if (child)
				children.emplace_back(*child, direction);
		}

		return children;
	}


private: // methods
	
	size_t GetBlankPosition() const
	{
		// TODO refactor using STL algo
		for (auto idx = 0u; idx < m_data.size(); ++idx)
		{
			if (m_data[idx] == 0)
				return idx;
		}
		throw std::runtime_error("Unexpected");
	}
	Position2D GetBlankPosition2D() const
	{
		auto&& absolute = GetBlankPosition();
		return { absolute / N, absolute % N };
	}

	// Perform the move if possible and return the state. Returns std::nullopt otherwise.

	std::optional<State> Move(MoveDirection direction) const
	{
		static const std::unordered_map < MoveDirection, std::function<std::optional<State>(const State&)>> directionToFunction
		{
			{MoveDirection::DOWN, std::mem_fn(&State::MoveDown)},
			{MoveDirection::UP, std::mem_fn(&State::MoveUp)},
			{MoveDirection::LEFT, std::mem_fn(&State::MoveLeft)},
			{MoveDirection::RIGHT, std::mem_fn(&State::MoveRight)}
		};

		auto pFunc = directionToFunction.at(direction);
		return pFunc(*this);
	}

	static State SwapTiles(const State& state, size_t firstPos, size_t secondPos)
	{
		auto childData = state.GetData();
		std::swap(childData[firstPos], childData[secondPos]);
		return { std::move(childData) };
	}

	std::optional<State> MoveRight() const
	{
		if (GetBlankPosition2D().second == 0) return std::nullopt;

		auto blankPosition = GetBlankPosition();
		return SwapTiles(*this, blankPosition, blankPosition - 1);
	}
	std::optional<State> MoveLeft() const
	{
		if (GetBlankPosition2D().second == N - 1) return std::nullopt;

		auto blankPosition = GetBlankPosition();
		return SwapTiles(*this, blankPosition, blankPosition + 1);
	}
	std::optional<State> MoveDown() const
	{
		if (GetBlankPosition2D().first == 0) return std::nullopt;

		auto blankPosition = GetBlankPosition();
		return SwapTiles(*this, blankPosition, blankPosition - N);
	}
	std::optional<State> MoveUp() const
	{
		if (GetBlankPosition2D().first == N - 1) return std::nullopt;

		auto blankPosition = GetBlankPosition();
		return SwapTiles(*this, blankPosition, blankPosition + N);
	}

private: // members
	Data m_data;
};

std::ostream& operator<< (std::ostream& os, MoveDirection dir)
{
	static const std::map<MoveDirection, const char*> namesMap
	{
		{ MoveDirection::LEFT, "l" },
		{ MoveDirection::RIGHT, "r" },
		{ MoveDirection::UP, "u" },
		{ MoveDirection::DOWN, "d" },
	};

	os << namesMap.at(dir);
	return os;
}

template <size_t N = 3>
std::ostream& operator<< (std::ostream& os, const State<N>& state)
{
	os << std::endl;
	for (size_t index = 0; index < state.GetData().size(); ++index)
	{
		os << static_cast<unsigned int>(state.GetData()[index]) << ' ';

		if (index % State<N>::Dimension == State<N>::Dimension - 1) os << std::endl;
	}
	os << std::endl;

	return os;
}

using State3X3 = State<3>;
using State4X4 = State<4>;
